import java.util.LinkedList;
//https://www.javatpoint.com/java-string-isempty
//https://home.cs.colorado.edu/~main/docs/edu/colorado/collections/DoubleStack.html
//http://www.mathcs.emory.edu/~cheung/Courses/171/Projects/hw4/hw4.html
//https://www.javatpoint.com/understanding-toString()-method

public class DoubleStack {


   public static void main (String[] argum) {
   }

   private final LinkedList<Double> doubleStack = new LinkedList<>();

   DoubleStack() {
   }
   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack doubleS = new DoubleStack();
      for (int i = 0; i <doubleStack.size(); i++) {
         doubleS.doubleStack.addLast(doubleStack.get(i));
      }
      return doubleS;
   }

   public boolean stEmpty() {
      return doubleStack.size() <= 0;
   }

   public void push (double a) {
      doubleStack.addFirst(a);
   }

   public double pop() {
      if (stEmpty()) {
         throw new RuntimeException("No elements");
      }
      return doubleStack.pop();
   }

   public void op (String s) throws RuntimeException {
      try {

         if ("+".equals(s)) {
            double LIFO = doubleStack.pop();
            double LE = doubleStack.pop();
            doubleStack.push(LIFO + LE);
         } else if ("-".equals(s)) {
            double LE = doubleStack.pop();
            double LIFO = doubleStack.pop();
            doubleStack.push(LIFO - LE);
         } else if ("*".equals(s)) {
            double LE = doubleStack.pop();
            double LIFO = doubleStack.pop();
            doubleStack.push(LIFO * LE);
         } else if ("/".equals(s)) {
            double LE = doubleStack.pop();
            double LIFO = doubleStack.pop();
            doubleStack.push(LIFO / LE);
         } else if ("SWAP".equals(s)) {
            if (doubleStack.size() < 2){
               throw new RuntimeException("Should be 2 el for SWAP");
            }
            double LE = doubleStack.pop();
            double LIFO = doubleStack.pop();
            doubleStack.push(LE);
            doubleStack.push(LIFO);
         } else if ("ROT".equals(s)) {
            if (doubleStack.size() < 3){
               throw new RuntimeException("Should be 3 el for ROT");
            }
            double LE = doubleStack.pop();
            double LIFO = doubleStack.pop();
            double LLE = doubleStack.pop();
            doubleStack.push(LIFO);
            doubleStack.push(LE);
            doubleStack.push(LLE);
         } else if ("DUP".equals(s)) {
            if (doubleStack.size() < 1){
               throw new RuntimeException("Should be 1 el");
            }
            double LE = doubleStack.pop();
            doubleStack.push(LE);
            doubleStack.push(LE);
         } else {
            System.out.printf("'%s' can not be operator, '+', '-', '*' or '/' just can be", s);
         }
      } catch (RuntimeException e) {
         throw new RuntimeException(String.format("Can not do operation %s", s));
      }
   }

   public double tos() {
      if (stEmpty()) {
         throw new RuntimeException("Empty");
      }
      return doubleStack.getFirst();
   }


   @Override
   public boolean equals (Object o) { //Возвращаем false, если размеры стека разные
      if (((DoubleStack) o).doubleStack.size() != doubleStack.size()) {
         return false;
      }

      for (int i = 0; i < doubleStack.size(); i++) { //Начинаем сравнивать оба DoubleStack один за другим. Вернуть false, если обнаружено несоответствие
         if (!((DoubleStack) o).doubleStack.get(i).equals(doubleStack.get(i))) {
            return false;
         }
      }
      return true; //Возвращаем истину, если несоответствия не обнаружено
   }

   @Override
   public String toString() {
      StringBuilder str = new StringBuilder();
      for (int i = doubleStack.size() - 1; i >= 0; i--)
         str.append(doubleStack.get(i)).append(" ");
      return str.toString();
   }

   public static double interpret (String pol) { //Удаляем все ненужное форматирование
      String empty = pol.replaceAll("\t", "").replaceAll("\n", "").replaceAll("[ ](?=[ ])","").trim();
      String[] polArray = empty.split(" ");// Разделить строку на массив, используя пробелы между ними в качестве разделителей
      if (empty.isEmpty()) {
         throw new RuntimeException(String.format("No Elements in  %s", pol));
      }


      DoubleStack resultStack = new DoubleStack();
      for (String s : polArray) {
         if (s.matches("[+*/\\-]") || s.equals("SWAP") ||
                 s.equals("ROT") || s.equals("DUP")) {
            resultStack.op(s);
         } else {
            resultStack.push(Double.parseDouble(s)); //Используем стек для вычислений
         }
      }

      if (resultStack.doubleStack.size() == 1) {
         return resultStack.pop();
      } else {
         throw new RuntimeException(String.format("Not possible to complate  %s", pol));// Возврат, только если размер стека равен 1,
         // Если не 1, значит, вычисления не завершены, числа остались и выдается исключение
      }
   }
}
 